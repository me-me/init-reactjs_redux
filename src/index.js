
import React from 'react';
import ReactDOM from "react-dom";

import { Provider }  from "react-redux";
import { ConnectedRouter as Router, routerReducer, routerMiddleware } from "react-router-redux";
import { createStore, applyMiddleware, combineReducers } from "redux";
import reducers from './store/configureStore';
import App from './App';
import  {composeWithDevTools}  from 'redux-devtools-extension';
import {HashRouter} from "react-router-dom";
import thunk from "redux-thunk";
import promiseMiddleware  from 'redux-promise'; 
import createHistory from 'history/createBrowserHistory';

const history = createHistory();
const middleware = routerMiddleware(history);


const store = createStore(reducers, composeWithDevTools(applyMiddleware(middleware, promiseMiddleware, thunk)));

ReactDOM.render(

  <Provider store={store}>
    <Router history={history}>
      <div>
          <HashRouter>
        <App />
        </HashRouter>
      </div>
    </Router>
  </Provider>,
  document.getElementById('root')
);