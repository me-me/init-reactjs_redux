import { combineReducers } from 'redux';
import auth from './userReducers/auth_reducer';


const rootReducer = combineReducers({
    auth
});


export default rootReducer;