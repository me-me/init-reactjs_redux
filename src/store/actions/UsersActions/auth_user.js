import axios from "axios/index";
import { browserHistory } from 'react-router';
import {ToastContainer, toast} from 'react-toastify';
import {
    AUTH_USER,
    UNAUTH_USER
} from '../ActionType';



export function authUser(email, password, notify) {
    
    return function (dispatch) {
        axios.post(process.env.REACT_APP_API_URL + "/users/login", {
        "credentials": {
            "email": email,
            "password": password
          },
    }, { 'content-type': 'application/x-www-form-urlencoded' } )
    .then(res => {
           if (res.data.code == "200") {
            localStorage.setItem('token', res.data.token);
            localStorage.setItem('userId', res.data.userId);
            dispatch({
            type: AUTH_USER,
          })
          browserHistory.push("/#/");
          window.location.href = window.location.href;
    } else {
        dispatch({
            type: UNAUTH_USER,
          })
          const notify = [
            toast.error("Email et/ou mot de pass incorrect", {
                position: toast.POSITION.RIGHT_CENTER
            })];
    }
    })
    .catch(err => {
        dispatch({
            type: UNAUTH_USER,
          })
          const notify = [
            toast.error("Email et/ou mot de pass incorrect", {
                position: toast.POSITION.RIGHT_CENTER
            })];
    });
}

};