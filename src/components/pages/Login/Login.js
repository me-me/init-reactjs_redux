
import React from "react";
import { connect } from 'react-redux';
import { authUser } from "../../../store/actions/UsersActions/auth_user";
import {ToastContainer, toast} from 'react-toastify';
import { bindActionCreators } from "redux";




class Login extends React.Component {
    constructor(props) {
        super(props);

        this.pstate = {
          password: "",
          email: "",
          notify: "",
        }

        this.handlFormSubmit = this.handlFormSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
      const target = event.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;

      this.setState({
          [name]: value
      });
  }
  handlFormSubmit(e) {
    e.preventDefault();
    this.props.authUser(this.state.email, this.state.password, this.state.notify);
  }

    render() {

      
      return (
            <div className="login-box">
            <ToastContainer/>
            <div className="login-logo">
              <a href="../../index2.html"><b>Admin</b>Community</a>
            </div>
             <div className="login-box-body">
              <p className="login-box-msg">Connectez-vous pour commencer votre session</p>
          
              <form onSubmit={this.handlFormSubmit} encType="multipart/form-data">
              
               <div className="form-group has-feedback">
                  <input onChange={this.handleChange} name="email" type="email" className="form-control" placeholder="Email" required/>
                  <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
              
                <div className="form-group has-feedback">
                  <input onChange={this.handleChange} name="password" type="password" className="form-control" placeholder="Password" required/>
                  <span className="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
              
                <div className="row">
                  <div className="col-xs-8">
                    <div className="checkbox icheck">
                      <label>
                        <input type="checkbox"/> Remember Me
                      </label>
                    </div>
                  </div>
                   <div className="col-xs-4">
                    <button   className="btn btn-primary btn-block btn-flat">Sign In</button>
                  </div>
                 </div>
                 
              </form> 
            </div>
           </div>
        );
    }
};


const mapStateToProps = state => {
  return {
    auth: state.auth,
  };
};


function mapDispatchToProps (dispatch) {
  return bindActionCreators({authUser}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);