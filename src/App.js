import  React, { Component } from "react";
import { Redirect, Link, BrowserRouter, Route, Switch } from "react-router-dom";
import { router, route, Browserhistory } from "react-router";
import Login from "./components/pages/Login/Login";
import decode from 'jwt-decode';
import NotFound from './components/pages/NotFound/NotFound';
require('dotenv').config();


class App extends Component {


    render() {
        const checkAuth = () => {
            const token = localStorage.getItem('token');
            if (!token) {
              return false;
            }
          
            try {
              // { exp: 12903819203 }
              const { exp } = decode(token);
          
              if (exp < new Date().getTime() / 1000) {
                return false;
              }
          
            } catch (e) {
              return false;
            }
          
            return true;
          }
          
          const AuthRoute = ({ component: Component, ...rest }) => (
            <Route {...rest} render={props => (
              checkAuth() ? (
                <Component {...props} />
              ) : (
                  <Redirect to={{ pathname: '/login' }} />
                )
            )} />
          )
        // Example of route protection ;   <AuthRoute exact path="/" component={Home} />
        return (
            <div className="row">
            <BrowserRouter>
                <Switch>
                    <Route exact path="/login" component={Login} />/>
                    <AuthRoute exact component={NotFound} />
                </Switch>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;