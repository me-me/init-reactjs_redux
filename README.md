REACTJS/REDUX BOILLERPLATE 
==========================

## Description of the project :
This is a React/Redux boilerplate with a login system integrated
and ADMIN-LTE template ressources added.

## Technologies :
 - ReactJs 16.3.2
 - Redux 4.0.0


## Project's Goals and objectives

## Git flow
There are three branches:
 - Master - origin
 - Staging - follow master
 - Develop - follow staging

 The *Master* branch is used for production. Only the features we know are perfectly working should be merged on *Master*  
The *Staging* branch is made for testing purposes. Once a feature is developed, it is merged on *Staging*.  
The *Dev* branch is where new features are developped.

## Git Commit messages guidlines

Commit messages should conform to the following rules:  
	- Title in capital letters  
	- The title is separated from the body of the message by one empty line  
	- A line should not be longer than 80 characters  
	- The message must focus on the WHY and WHAT, not HOW.  

This template can be used for the commit messages:  

> COMMIT MESSAGE TITLE
> 
> Here, I explain WHAT I did (the improvements I made to the code, what I removed
> from it, etc...)
> I alos explain WHY I did it.


## Build the project

Clone this project

```bash
https://me-me@bitbucket.org/me-me/init-reactjs_redux.git
```

Edit your `/etc/hosts` file:

```
127.0.0.1   front.tcm
```

- Copy the environment variables

```bash
# you may need to use sudo
cp .env-template .env
```

Build the containers

```bash
# Build and start the docker containers
sudo docker-compose up --build
```

The application will be served on port 3000

## Redux 
The application is bundled on redux debug mode - check in your 
navigator in inspect mode.

## Folders structure

```bash
├── docker-compose.yml
├── package.json
├── public
│   ├── bundle.js
│   ├── favicon.ico
│   ├── index.html
│   └── manifest.json
├── README.md
├── src
│   ├── App.js
│   ├── App.test.js
│   ├── components
│   │   ├── common
│   │   └── pages
│   │       ├── Login
│   │       │   └── Login.js
│   │       └── NotFound
│   │           └── NotFound.js
│   ├── index.css
│   ├── index.js
│   ├── registerServiceWorker.js
│   └── store
│       ├── actions
│       │   ├── ActionType.js
│       │   ├── index.js
│       │   └── UsersActions
│       │       └── auth_user.js
│       ├── configureStore.js
│       └── reducers
│           ├── index.js
│           └── userReducers
│               └── auth_reducer.js
└── webpack.config.js
```

### Help

Stop and remove all containers

```bash
docker stop $(docker ps -a -q)
```

Connect to a container via bash (get the container name you want to connect to via command `docker ps`)
```bash
docker exec -ti containername bash
```

Execute a command directly in a container without connecting in bash (get the container name you want to connect to via command `docker ps`)

```bash
docker exec -i containername yourcommand
```

Delete all inages 

```bash
docker rmi -f $(docker images -q)
```

Show images 

```bash
docker images
```


## Maintainer :
 - ABN - contact@naceur-abdeljalil.com 
```
